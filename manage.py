#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

from dotenv import load_dotenv


def main():
    """Run administrative tasks."""
    load_dotenv()
    env = os.getenv("DJANGO_SETTINGS")
    settings_module = f"cosmo_core.settings.{env}"
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        message = """
                Couldn't import Django. Are you sure it's installed and
                available on your PYTHONPATH environment variable? Did you
                forget to activate a virtual environment?
                """
        raise ImportError(message) from exc
    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    main()
