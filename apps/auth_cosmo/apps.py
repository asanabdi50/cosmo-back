from django.apps import AppConfig


class CinemaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "auth_cosmo"

    def ready(self) -> None:
        self._setup_di()

    def _setup_di(self) -> None:
        from .container import Container

        container = Container()
        container.wire(
            modules=[
                "auth_cosmo.accounts.views.auth_view",
                "auth_cosmo.accounts.views.user_view",
            ]
        )
