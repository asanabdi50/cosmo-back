from django.conf import settings
from django.core.mail import send_mail

from cosmo_core.celery import app


@app.task
def send_account_resert_password_link(email: str, confirmation_token: str):
    subject = """Сброс пароля на shows.kg"""
    link = f"{settings.ACTIVATION_LINK_DOMAIN}/reset-password/input-data?user_email={email}&confirmation_token={confirmation_token}"
    content = f"""
Ваше имя пользователя, если вы забыли: {email}
Пожалуйста, перейдите по ссылке и создайте новый пароль:!!!

{link}

Спасибо за использование нашей платформы!

Команда Zetta42
    """
    send_mail(
        subject=subject.replace("\n", ""),
        message=content,
        recipient_list=[email],
        from_email=settings.DEFAULT_FROM_EMAIL,
        fail_silently=False,
    )
    return {"message": "send email for reset password"}


@app.task
def send_account_update_password(email):
    subject = """Сброс пароля на shows.kg"""
    content = f"""
Ваше имя пользователя, если вы забыли: {email}

Ваш пароль был изменен!!!

Спасибо за использование нашей платформы!

Команда Zetta42
"""
    send_mail(
        subject=subject.replace("\n", ""),
        message=content,
        recipient_list=[email],
        from_email=settings.DEFAULT_FROM_EMAIL,
        fail_silently=False,
    )
