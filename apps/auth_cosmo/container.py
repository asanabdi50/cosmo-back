from dependency_injector import containers, providers

from auth_cosmo.accounts.containers import AuthenticateContainer, UserContainer


def get_config() -> list:
    # Implement env based config loading
    return ["config.yaml"]


class Container(containers.DeclarativeContainer):
    config = providers.Configuration()

    user_package = providers.Container(
        UserContainer,
        config=config,
    )
    authenticate_package = providers.Container(
        AuthenticateContainer,
        config=config,
    )
