import logging
from dataclasses import asdict

from django.conf import settings
from django.contrib.auth.tokens import default_token_generator

from auth_cosmo.accounts.models import User
from auth_cosmo.accounts.types import UserDataCreate, UserDataObject, UserEmail, UserUpdateData
from auth_cosmo.utils.exceptions import RepositoryException

logger = logging.getLogger(__name__)


class UserRepositoryError(RepositoryException):
    class Code:
        EXISTS_WITH_SAVE_USERNAME = "EXISTS WITH SAVE USERNAME"


class UserRepository:
    # responsibility: database access
    def create_user(self, user_data: UserDataCreate) -> UserDataObject:
        user = User.objects.create(
            email=user_data.email,
            username=user_data.username,
            is_client=True,
            is_active=False,
        )
        user.set_password(user_data.password)
        user.save()
        return self._build_user_dto(user)

    def get_user_by_email(self, email: UserEmail) -> UserDataObject:
        try:
            user = User.objects.get(email=email)
            return self._build_user_dto(user)
        except User.DoesNotExist:
            return None

    def get_user_by_username(self, username: UserEmail) -> UserDataObject:
        try:
            user = User.objects.get(username=username)
            return self._build_user_dto(user)
        except User.DoesNotExist:
            return None

    def change_password(self, email: UserEmail, password: str) -> None:
        user = User.objects.get(email=email)
        user.set_password(password)
        user.save()

    def generage_confirmation_token(self, email: UserEmail) -> str:
        user = User.objects.get(email=email)
        confirmation_token = default_token_generator.make_token(user)
        return confirmation_token

    def update_user_data(self, user_data: UserUpdateData, email: UserEmail) -> None:
        try:
            user_dict = asdict(user_data)
            clear_user_dict = {k: v for k, v in user_dict.items() if v is not None}
            User.objects.filter(email=email).update(**clear_user_dict)
        except Exception as e:
            logger.info("Error:  %s", e)
            raise UserRepositoryError(UserRepositoryError.Code.EXISTS_WITH_SAVE_USERNAME) from e

    def _build_user_dto(self, user: User) -> UserDataObject:
        return UserDataObject(
            id=user.id,
            email=user.email,
            username=user.username,
            user_avatar=f"{settings.IMAGE_LINK_DOMAIN}{user.user_avatar.url}"
            if user.user_avatar
            else None,
            auth_provider=user.auth_provider,
        )
