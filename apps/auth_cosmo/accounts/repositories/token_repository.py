from auth_cosmo.accounts.models import UserToken
from auth_cosmo.accounts.types import ActivationTokenCreateData


class TokenRepository:
    def create_activaten_token(self, activation_token_data: ActivationTokenCreateData) -> None:
        token_object = UserToken.objects.create(
            user_id=activation_token_data.user_id,
            token=activation_token_data.token,
            token_type=activation_token_data.token_type,
            timestamp=activation_token_data.timestamp,
            valid_through=activation_token_data.valid_through,
        )
        token_object.save()
