from io import BytesIO

import requests
from django.conf import settings
from django.core.files import File
from django.db.models import Q
from django.utils import timezone
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken

from auth_cosmo.accounts.models import User
from auth_cosmo.accounts.types import GoogleSocialAuthDataCreate, UserAuthenticationDataObject


class AuthenticateRepository:
    def authenticate_by_email(self, email: str, password: str) -> UserAuthenticationDataObject:
        user = self._authenticate("email__iexact", email, password)
        if user is not None:
            token = self._create_user_token(user)
            return self._build_user_authentication_dto(
                user, token.get("access"), token.get("refresh")
            )
        return None

    def _authenticate(self, lookup_field, value, password):
        try:
            user = User.objects.get(**{lookup_field: value})
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None

    def _create_user_token(self, user: User) -> dict:
        user.last_login = timezone.now()
        user.save(update_fields=["last_login"])
        return {
            "access": str(AccessToken.for_user(user)),
            "refresh": str(RefreshToken.for_user(user)),
        }

    def _build_user_authentication_dto(
        self, user: User, access: str, refresh: str
    ) -> UserAuthenticationDataObject:
        return UserAuthenticationDataObject(
            id=user.id,
            email=user.email,
            username=user.username,
            user_avatar=f"{settings.IMAGE_LINK_DOMAIN}{user.user_avatar.url}"
            if user.user_avatar
            else None,
            auth_provider=user.auth_provider,
            access=access,
            refresh=refresh,
        )

    def _create_user_social(self, user_data: GoogleSocialAuthDataCreate):
        get_user_name = user_data.email.split("@")[0]
        user_avatar_data = requests.get(user_data.picture_url, timeout=10).content
        user = User.objects.create(
            first_name=user_data.first_name,
            last_name=user_data.last_name,
            email=user_data.email,
            username=get_user_name,
            is_client=True,
            auth_provider="google",
            user_avatar=File(BytesIO(user_avatar_data), name=f"{get_user_name}.png"),
        )
        user.save()
        return user

    def authenticate_social_user(
        self, user_data: GoogleSocialAuthDataCreate
    ) -> UserAuthenticationDataObject:
        user_object = User.objects.filter(
            Q(email=user_data.email) & Q(auth_provider=user_data.auth_provider)
        ).first()
        if user_object:
            token = self._create_user_token(user_object)
            return self._build_user_authentication_dto(
                user_object, token.get("access"), token.get("refresh")
            )
        new_user = self._create_user_social(user_data)
        token = self._create_user_token(new_user)
        return self._build_user_authentication_dto(
            new_user, token.get("access"), token.get("refresh")
        )
