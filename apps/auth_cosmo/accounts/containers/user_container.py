from dependency_injector import providers
from dependency_injector.containers import DeclarativeContainer

from auth_cosmo.accounts.repositories import UserRepository
from auth_cosmo.accounts.services import UserService


class UserContainer(DeclarativeContainer):
    config = providers.Configuration()

    user_repository: providers.Dependency[UserRepository] = providers.Singleton(
        UserRepository,
    )

    user_service: providers.Dependency[UserService] = providers.Singleton(
        UserService,
        config=config,
        user_repository=user_repository,
    )
