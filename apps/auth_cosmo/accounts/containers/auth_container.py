from dependency_injector import providers
from dependency_injector.containers import DeclarativeContainer

from auth_cosmo.accounts.repositories import AuthenticateRepository
from auth_cosmo.accounts.services import AuthenticationService


class AuthenticateContainer(DeclarativeContainer):
    config = providers.Configuration()

    authenticate_repository: providers.Dependency[AuthenticateRepository] = providers.Singleton(
        AuthenticateRepository,
    )

    authenticate_service: providers.Dependency[AuthenticationService] = providers.Singleton(
        AuthenticationService,
        config=config,
        authenticate_repository=authenticate_repository,
    )
