from rest_framework import serializers


class AuthenticationSerializer(serializers.Serializer):
    id = serializers.UUIDField()
    username = serializers.CharField()
    email = serializers.CharField()
    user_avatar = serializers.URLField(allow_null=True, required=False)
    auth_provider = serializers.CharField()
    access = serializers.CharField()
    refresh = serializers.CharField()


class GoogleSocialAuthSerializer(serializers.Serializer):
    auth_token = serializers.CharField(required=True)


class UserAuthenticationSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()
