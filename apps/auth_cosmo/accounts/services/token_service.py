import logging

from auth_cosmo.utils.exceptions import ServiceException

logger = logging.getLogger(__name__)


class TokenServerError(ServiceException):
    class Code:
        TOKEN_IS_NOT_VALID = "TOKEN_IS_NOT_VALID"
        TOKEN_IS_EXPIRED = "TOKEN_IS_EXPIRED"


class TokenServive:
    def __init__(self, config):
        pass
