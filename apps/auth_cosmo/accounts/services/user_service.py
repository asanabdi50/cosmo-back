import logging

from django.contrib.auth.tokens import default_token_generator

from auth_cosmo.accounts.repositories import UserRepository
from auth_cosmo.accounts.types import UserDataCreate, UserDataObject, UserEmail
from auth_cosmo.tasks import send_account_resert_password_link, send_account_update_password
from auth_cosmo.utils.exceptions import ServiceException

logger = logging.getLogger(__name__)


class UserServiceError(ServiceException):
    class Code:
        USER_NOT_FOUND = "USER_NOT_FOUND"
        USER_EXISTS_WITH_SAME_EMAIL = "USER_EXISTS_WITH_SAME_EMAIL"
        USER_EXISTS_WITH_SAME_USERNAME = "USER_EXISTS_WITH_SAME_USERNAME"
        TOKEN_EXPIRED_MESSAGE = "TOKEN_EXPIRED"  # noqa: S105


class UserService:
    # responsibility: business logic
    def __init__(
        self,
        config: dict,
        user_repository: UserRepository,
    ) -> None:
        self._config = config
        self._user_repository = user_repository

    def get_user_by_email(self, email: UserEmail) -> UserDataObject:
        user = self._user_repository.get_user_by_email(email)
        if not user:
            logger.exception("User not found with email %s", email)
            raise UserServiceError(UserServiceError.Code.USER_NOT_FOUND)
        user = self._user_repository.get_user_by_email(email)
        return user

    def create_user(self, user_object: UserDataCreate) -> UserDataObject:
        user_email = self._user_repository.get_user_by_email(user_object.email)
        if user_email:
            logger.exception("User exists with same email %s", user_object.email)
            raise UserServiceError(UserServiceError.Code.USER_EXISTS_WITH_SAME_EMAIL)
        user_username = self._user_repository.get_user_by_username(user_object.username)
        if user_username:
            logger.exception("User exists with same username %s", user_object.username)
            raise UserServiceError(UserServiceError.Code.USER_EXISTS_WITH_SAME_USERNAME)
        user = self._user_repository.create_user(user_object)
        logger.info("User created %s", user.email)
        return user

    def email_send_reset_password(self, email: UserEmail) -> None:
        user = self._user_repository.get_user_by_email(email)
        if not user:
            logger.exception("User not found with email %s", email)
            raise UserServiceError(UserServiceError.Code.USER_NOT_FOUND)
        confirmation_token = self._user_repository.generage_confirmation_token(email)
        send_account_resert_password_link.delay(email, confirmation_token)
        logger.info("User confirmation token %s", confirmation_token)

    def change_password(self, email: UserEmail, password: str, token: str) -> None:
        user = self._user_repository.get_user_by_email(email)
        if not user:
            logger.exception("User not found with email %s", email)
            raise UserServiceError(UserServiceError.Code.USER_NOT_FOUND)
        if not default_token_generator.check_token(user, token):
            logger.exception("Token is invalid or expired %s", token)
            raise UserServiceError(UserServiceError.Code.TOKEN_EXPIRED_MESSAGE)
        self._user_repository.change_password(email, password)
        send_account_update_password.delay(email)
        logger.info("User password changed %s", email)

    def update_user_data(self, user_data: UserDataCreate, email: UserEmail) -> UserDataObject:
        user = self._user_repository.get_user_by_email(email)
        if not user:
            logger.exception("User not found with email %s", email)
            raise UserServiceError(UserServiceError.Code.USER_NOT_FOUND)
        self._user_repository.update_user_data(user_data=user_data, email=email)
        return user
