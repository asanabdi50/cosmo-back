import datetime
import uuid
from dataclasses import dataclass
from typing import NewType, Optional

from django.core.files.uploadedfile import InMemoryUploadedFile

UserId = NewType("UserId", uuid.UUID)
UserEmail = NewType("UserEmail", str)


@dataclass(frozen=True)
class UserDataCreate:
    email: UserEmail
    username: str
    password: str


@dataclass(frozen=True)
class UserDataObject:
    id: UserId
    email: UserEmail
    username: str
    user_avatar: str
    auth_provider: str


@dataclass(frozen=True)
class UserAuthenticationDataCreate:
    email: UserEmail
    password: str


@dataclass(frozen=True)
class UserAuthenticationDataObject:
    id: UserId
    email: UserEmail
    username: str
    user_avatar: str
    auth_provider: str
    access: str
    refresh: str


@dataclass(frozen=True)
class GoogleSocialAuthDataCreate:
    first_name: str
    last_name: str
    email: UserEmail
    picture_url: str
    auth_provider: str = "google"


@dataclass(frozen=True)
class UserUpdateData:
    username: Optional[str]
    user_avatar: Optional[InMemoryUploadedFile] = None


@dataclass(frozen=True)
class ActivationTokenCreateData:
    user_id: UserId
    email: UserEmail
    token: str
    token_type: str
    timestamp: datetime
    valid_through: datetime
