from django.urls import include, path

urlpatterns = [
    path("account/", include("auth_cosmo.accounts.urls.user_url")),
    path("authenticate/", include("auth_cosmo.accounts.urls.auth_url")),
]
