class ServiceException(Exception):
    pass


class RepositoryException(Exception):
    pass
