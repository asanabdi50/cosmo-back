from typing import ClassVar

from django.contrib import admin
from django.contrib.auth.models import Group

from .accounts.models import User

admin.site.unregister(Group)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display: ClassVar[list[str]] = [
        "id",
        "username",
        "email",
        "first_name",
        "last_name",
        "is_active",
        "is_staff",
        "is_superuser",
    ]
    list_filter: ClassVar[list[str]] = [
        "is_active",
        "is_staff",
        "is_superuser",
    ]
    search_fields: ClassVar[list[str]] = [
        "username",
        "email",
        "first_name",
        "last_name",
    ]
