from dataclasses import dataclass
from .models import Brand , Product , ProductColor , ProductVolume , ProductVariant
from typing import List, Literal
from django.contrib.auth import get_user_model

User = get_user_model()


@dataclass(frozen=True)
class ProductDataObject:
    article: str
    title: str
    description: str
    spec_description: str
    gender: Literal['Male', 'Female', 'Unisex']
    brands: List[Brand]


@dataclass(frozen=True)
class ProductVariantDataObjects:
    id: int
    product: Product
    color: ProductColor
    size: ProductVolume
    price: float
    addition_price: float


@dataclass(frozen=True)
class ProductVariantImageDataObjects:
    id: int
    image: str 
    product: Product
    created_at: str


@dataclass(frozen=True)
class ProductColorDataObjects:
    id: int
    hex_code: str
    name: str


@dataclass(frozen=True)
class ProductVolumeDataObjects:
    id: int
    volume_type: str
    volume_value: str


@dataclass(frozen=True)
class BrandDataObjects:
    title: str
    logo: str
    description: str


@dataclass(frozen=True)
class ProductReviewDataObjects:
    user: User
    product: List[Product]
    rating: int
    comment: str

@dataclass(frozen=True)
class ProductReviewQueryset:
    queryset: List[Product]


@dataclass(frozen=True)
class BrandDataObject:
    title: str
    logo: str
    description: str

