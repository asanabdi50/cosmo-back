from django.db.models.query import QuerySet
from typing import List

from .models import (
    Product,
    ProductVolume,
    ProductColor,
    ProductReview,
    ProductVariant,
    ProductVariantImage
)
from .types import (
    ProductColorDataObjects,
    BrandDataObject,
    ProductDataObject,
    ProductVolumeDataObjects,
    ProductReviewDataObjects,
    ProductVariantDataObjects,
    ProductVariantImageDataObjects
)
from django.contrib.auth import get_user_model

User = get_user_model()


class ProductRespository:
    def create_product(self, data: dict) -> ProductDataObject:
        return Product.objects.create(data=data)

    def get_all_products(self) -> QuerySet[Product]:
        return Product.objects.all()

    def get_product_by_title(self, product_title: str) -> ProductDataObject | None:
        try:
            product = Product.objects.get(title=product_title)
            return self._build_product_dto(product)
        except Product.DoesNotExist:
            return None
    
    def get_product_by_id(self, id: int) -> ProductDataObject | None:
        try:
            product = Product.objects.get(id=id)
            return self._build_product_dto(product)
        except Product.DoesNotExist:
            return None

    def update_product(self, data: dict, instance: Product) -> ProductDataObject:
        for key, value in data.items():
            setattr(instance, key, value)
        instance.save()
        return instance

    def delete_product(self, instance: Product) -> None:
        instance.delete()

    def _build_product_dto(self, product: Product) -> ProductDataObject:
        return ProductDataObject(
            article=product.article,
            title=product.title,
            description=product.description,
            spec_description=product.spec_description,
            gender=product.spec_description,
            brands=product.brands.all(),
        )


class ProductReviewRepository:
    def create_product_review(self, data: dict) -> ProductReviewDataObjects:
        product_review  = ProductReview.objects.create(data=data)
        return self._build_product_review_dto(product_review)

    def get_all_product_reviews(self) -> QuerySet[ProductReview]:
        return ProductReview.objects.all()

    def get_product_review_by_product_user(self, product: Product, user: User) -> ProductReviewDataObjects | None:
        try:
            product_review = ProductReview.objects.get(product=product, user=user)
            return self._build_product_review_dto(product_review)
        except ProductReview.DoesNotExist:
            return None
    
    def get_product_review_by_id(self, id: int) -> ProductReviewDataObjects | None:
        try:
            product_review = ProductReview.objects.get(id=id)
            return self._build_product_review_dto(product_review)
        except ProductReview.DoesNotExist:
            return None

    def update_product_review(self, data: dict, instance: ProductReview) -> ProductReviewDataObjects:
        for key, value in data.items():
            setattr(instance, key, value)
        instance.save()
        return instance

    def delete_product_review(self, instance: ProductReview) -> None:
        instance.delete()

    def _build_product_review_dto(self, productReview: ProductReview) -> ProductReviewDataObjects:
        return ProductReviewDataObjects(
            user=productReview.User,
            product=productReview.product,
            reating=productReview.rating,
            comment=productReview.comment
        )


class ProductVariantRepository:
    def create_product_variant(self, data: dict) -> ProductVariantDataObjects:
        product_variant  = ProductVariant.objects.create(data=data)
        return self._build_product_variant_dto(product_variant)

    def get_all_product_variants(self) -> QuerySet[ProductVariant]:
        return ProductVariant.objects.all()

    def get_product_variants_by_product(self, product: Product) -> List[ProductVariantDataObjects] | None:
        try:
            product_variants = ProductVariant.objects.filter(product=product)
            product_variants_dto = []
            for variant in product_variants:
                product_variants_dto.append(self._build_product_variant_dto(variant))
            return product_variants_dto
        except:
            return None
        
    def get_product_variant_by_product_color_size(self, product: Product, size: ProductVolume, color: ProductColor) -> ProductVariantDataObjects | None:
        try:
            product_variant = ProductVariant.objects.get(product=product, size=size, color=color)
            return self._build_product_variant_dto(product_variant)
        except ProductVariant.DoesNotExist:
            return None
    
    def get_product_variant_by_id(self, id: int) -> ProductVariantDataObjects | None:
        try:
            product_variant = ProductVariant.objects.get(id=id)
            return self._build_product_variant_dto(product_variant)
        except ProductVariant.DoesNotExist:
            return None

    def update_product_variant(self, data: dict, instance: ProductVariant) -> ProductVariantDataObjects:
        for key, value in data.items():
            setattr(instance, key, value)
        instance.save()
        return instance

    def delete_product_variant(self, instance: ProductVariant) -> None:
        instance.delete()

    def _build_product_variant_dto(self, productVariant: ProductVariant) -> ProductVariantDataObjects:
        return ProductVariantDataObjects(
            id=productVariant.id,
            product=productVariant.product,
            color=productVariant.color,
            size=productVariant.size,
            price=productVariant.price,
            addition_price=productVariant.addition_price
        )
    

class ProductVariantImageRepository:
    def create_product_variant_image(self, data: dict) -> ProductVariantImageDataObjects:
        product_variant_image  = ProductVariantImage.objects.create(data=data)
        return self._build_product_variant_image_dto(product_variant_image)

    def get_all_product_variant_images(self) -> QuerySet[ProductVariantImage]:
        return ProductVariantImage.objects.all()

    def get_product_variant_images_by_product(self, product: Product) -> List[ProductVariantImageDataObjects] | None:
        try:
            product_variant_images = ProductVariantImage.objects.filter(product=product)
            if not product_variant_images:
                raise FileNotFoundError
            product_variant_images_dto = []
            for variant in product_variant_images:
                product_variant_images_dto.append(self._build_product_variant_image_dto(variant))
            return product_variant_images_dto
        except:
            return None
    
    def get_product_variant_image_by_product_created_at(self, product: Product, created_at: str) -> ProductVariantImageDataObjects | None:
        try:
            product_variant_image = ProductVariantImage.objects.get(product=product, created_at=created_at)
            return self._build_product_variant_image_dto(product_variant_image)
        except ProductVariantImage.DoesNotExist:
            return None

    def get_product_variant_image_by_id(self, id: int) -> ProductVariantImageDataObjects | None:
        try:
            product_variant_image = ProductVariantImage.objects.get(id=id)
            return self._build_product_variant_image_dto(product_variant_image)
        except ProductVariantImage.DoesNotExist:
            return None

    def update_product_variant_image(self, data: dict, instance: ProductVariantImage) -> ProductVariantImageDataObjects:
        for key, value in data.items():
            setattr(instance, key, value)
        instance.save()
        return instance

    def delete_product_variant_image(self, instance: ProductVariantImage) -> None:
        instance.delete()

    def _build_product_variant_image_dto(self, productVariantImage: ProductVariantImage) -> ProductVariantImageDataObjects:
        return ProductVariantImageDataObjects(
            id=productVariantImage.id,
            image=productVariantImage.image,
            product=productVariantImage.product,
            created_at=productVariantImage.created_at
        )


class ProductColorRepository:
    def create_product_color(self, data: dict) -> ProductColorDataObjects:
        product_color  = ProductColor.objects.create(data=data)
        return self._build_product_color_dto(product_color)

    def get_all_product_colors(self) -> QuerySet[ProductColor]:
        return ProductColor.objects.all()

    def get_product_color_name(self, name: str) -> ProductColorDataObjects | None:
        try:
            product_color = ProductColor.objects.filter(name=name)
            return self._build_product_color_dto(product_color)
        except ProductColor.DoesNotExist:
            return None
    
    def get_product_color_by_id(self, id: int) -> ProductColorDataObjects | None:
        try:
            product_color = ProductColor.objects.get(id=id)
            return self._build_product_color_dto(product_color)
        except ProductColor.DoesNotExist:
            return None

    def update_product_color(self, data: dict, instance: ProductColor) -> ProductColorDataObjects:
        for key, value in data.items():
            setattr(instance, key, value)
        instance.save()
        return instance

    def delete_product_color(self, instance: ProductColor) -> None:
        instance.delete()

    def _build_product_color_dto(self, productColor: ProductColor) -> ProductColorDataObjects:
        return ProductColorDataObjects(
            id=productColor.id,
            hex_code=productColor.hex_code,
            name=productColor.name
        )


class ProductVolumeRepository:
    def create_product_volume(self, data: dict) -> ProductVolumeDataObjects:
        product_volume  = ProductVolume.objects.create(data=data)
        return self._build_product_volume_dto(product_volume)

    def get_all_product_volumes(self) -> QuerySet[ProductVolume]:
        return ProductVolume.objects.all()

    def get_product_volume_volume_type(self, volume_type: str) -> ProductVolumeDataObjects | None:
        try:
            product_volume = ProductVolume.objects.filter(volume_type=volume_type)
            return self._build_product_volume_dto(product_volume)
        except ProductVolume.DoesNotExist:
            return None
    
    def get_product_volume_by_id(self, id:int):
        try:
            product_volume = ProductVolume.objects.get(id=id)
            return self._build_product_volume_dto(product_volume)
        except ProductVolume.DoesNotExist:
            return None

    def update_product_volume(self, data: dict, instance: ProductVolume) -> ProductVolumeDataObjects:
        for key, value in data.items():
            setattr(instance, key, value)
        instance.save()
        return instance

    def delete_product_volume(self, instance: ProductVolume) -> None:
        instance.delete()

    def _build_product_volume_dto(self, productVolume: ProductVolume) -> ProductColorDataObjects:
        return ProductVolumeDataObjects(
            id=productVolume.id,
            volume_type=productVolume.volume_type,
            volume_value=productVolume.volume_value
        )
