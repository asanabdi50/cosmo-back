from dependency_injector import providers
from dependency_injector.containers import DeclarativeContainer

from .repository import (
    ProductRespository,
    ProductColorRepository,
    ProductReviewRepository,
    ProductVolumeRepository,
    ProductVariantRepository,
    ProductVariantImageRepository
)
from .service import (
    ProductService,
    ProductColorService,
    ProductReviewService,
    ProductVolumeService,
    ProductVariantService,
    ProductVariantImageService
)


class ProductContainer(DeclarativeContainer):
    config = providers.Configuration()

    product_repository: providers.Dependency[ProductRespository] = providers.Singleton(
        ProductRespository,
    )

    product_service: providers.Dependency[ProductService] = providers.Singleton(
        ProductService,
        config=config,
        user_repository=product_repository,
    )

class ProductReviewContainer(DeclarativeContainer):
    config = providers.Configuration()

    product_review_repository: providers.Dependency[ProductReviewRepository] = providers.Singleton(
        ProductReviewRepository,
    )

    product_review_service: providers.Dependency[ProductReviewService] = providers.Singleton(
        ProductReviewService,
        config=config,
        user_repository=product_review_repository,
    )

class ProductColorContainer(DeclarativeContainer):
    config = providers.Configuration()

    product_color_repository: providers.Dependency[ProductColorRepository] = providers.Singleton(
        ProductColorRepository,
    )

    user_service: providers.Dependency[ProductColorService] = providers.Singleton(
        ProductColorService,
        config=config,
        user_repository=product_color_repository,
    )

class ProductVariantImageContainer(DeclarativeContainer):
    config = providers.Configuration()

    product_variant_image_repository: providers.Dependency[ProductVariantImageRepository] = providers.Singleton(
        ProductVariantImageRepository,
    )

    user_service: providers.Dependency[ProductVariantImageService] = providers.Singleton(
        ProductVariantImageService,
        config=config,
        user_repository=product_variant_image_repository,
    )

class ProductVariantContainer(DeclarativeContainer):
    config = providers.Configuration()

    product_variant_repository: providers.Dependency[ProductVariantRepository] = providers.Singleton(
        ProductVariantRepository,
    )

    user_service: providers.Dependency[ProductVariantService] = providers.Singleton(
        ProductVariantService,
        config=config,
        user_repository=product_variant_repository,
    )

class ProductVolumeContainer(DeclarativeContainer):
    config = providers.Configuration()

    product_volume_repository: providers.Dependency[ProductVolumeRepository] = providers.Singleton(
        ProductVolumeRepository,
    )

    user_service: providers.Dependency[ProductVolumeService] = providers.Singleton(
        ProductVolumeService,
        config=config,
        user_repository=product_volume_repository,
    )