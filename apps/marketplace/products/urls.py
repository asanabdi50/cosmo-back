from django.urls import path

from .views import (
    ProductAPIView,
    ProductReviewAPIView
)


urlpatterns = [
    path("products/", ProductAPIView.as_view(), name='product'),
    path("produt-review/", ProductReviewAPIView.as_view(), name='product-review')
]
