from dependency_injector import containers, providers

from .product_container import (
    ProductContainer,
    ProductColorContainer,
    ProductReviewContainer,
    ProductVariantContainer,
    ProductVariantImageContainer,
    ProductVolumeContainer
)


def get_config() -> list:
    # Implement env based config loading
    return ["config.yaml"]


class Container(containers.DeclarativeContainer):
    config = providers.Configuration()

    product_package = providers.Container(
        ProductContainer,
        config=config,
    )
    product_color_package = providers.Container(
        ProductColorContainer,
        config=config,
    )
    product_variant_package = providers.Container(
        ProductVariantContainer,
        config=config,
    )
    product_volume_package = providers.Container(
        ProductVolumeContainer,
        config=config,
    )
    product_variant_image_package = providers.Container(
        ProductVariantImageContainer,
        config=config,
    )
    product_review_package = providers.Container(
        ProductReviewContainer,
        config=config,
    )
