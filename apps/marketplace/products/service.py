from django.db.models import QuerySet
from typing import List

from .models import (
    Product,
    ProductVolume,
    ProductColor,
    ProductReview,
    ProductVariant,
    ProductVariantImage
)
from .repository import (
    ProductRespository,
    ProductVariantRepository,
    ProductReviewRepository,
    ProductVariantImageRepository,
    ProductColorRepository,
    ProductVolumeRepository
)
from .types import (
    ProductColorDataObjects,
    ProductReviewQueryset,
    BrandDataObject,
    ProductReviewDataObjects,
    ProductDataObject,
    ProductVolumeDataObjects,
    ProductVariantDataObjects,
    ProductVariantImageDataObjects
)
from django.contrib.auth import get_user_model

User = get_user_model()

#todo: Посмотреть все эксепшны на соотвествие полей и названий

class ProductError(Exception):
    class Code:
        PRODUCT_NOT_FOUND = "PRODUCT_NOT_FOUND"
        PRODUCT_EXISTS_WITH_SAME_TITLE = "PRODUCT_EXISTS_WITH_SAME_TITLE"


class ProductService:
    def __init__(
        self,
        repository: ProductRespository,
    ) -> None:
        self.repository = repository

    def get_all_products(self) -> QuerySet[Product]:
        return self.repository.get_all_products()

    def get_product(self, product_title: str) -> ProductDataObject:
        product = self.repository.get_product_by_title(product_title)
        if not product:
            return ProductError(ProductError.Code.PRODUCT_NOT_FOUND)
        return product
    
    def get_product_by_id(self, id: int) -> ProductDataObject:
        product = self.repository.get_product_by_id(id)
        if not product:
            return ProductError(ProductError.Code.PRODUCT_NOT_FOUND)
        return product

    def create_product(self, data: dict) -> ProductDataObject:
        product = self.repository.get_product_by_title(data.title)
        if product:
            return ProductError(ProductError.Code.PRODUCT_EXISTS_WITH_SAME_TITLE)
        product = self.repository.create_product(data)
        return product

    def update_product(self, data: dict, title: str) -> ProductDataObject:
        product_to_update = self.repository.get_product_by_title(title)
        if not product_to_update:
            return ProductError(ProductError.Code.PRODUCT_NOT_FOUND)
        updated_product = self.repository.update_product(data, product_to_update)
        return updated_product

    def delete_product(self, product: Product) -> None:
        self.repository.delete_product(product)


class ProductReviewError(Exception):
    class Code:
        PRODUCT_REVIEW_NOT_FOUND = "PRODUCT_REVIEW_NOT_FOUND"
        PRODUCT_REVIEW_EXISTS_WITH_SAME_USER_AND_COMMENT = "PRODUCT_REVIEW_EXISTS_WITH_SAME_USER_AND_COMMENT"


class ProductReviewService:
    def __init__(
        self,
        repository: ProductReviewRepository,
    ) -> None:
        self.repository = repository

    def get_all_productReviews(self) -> ProductReviewQueryset:
        return self.repository.get_all_product_reviews()

    def get_product_review(self, user: User, product: Product) -> ProductReviewDataObjects:
        productReview = self.repository.get_product_review_by_product_user(user=user, product=product)
        if not productReview:
            return ProductReviewError(ProductReviewError.Code.PRODUCT_REVIEW_NOT_FOUND)
        return productReview

    def get_product_review_by_id(self, id: int) -> ProductReviewDataObjects:
        product = self.repository.get_product_review_by_id(id)
        if not product:
            return ProductReviewError(ProductReviewError.Code.PRODUCT_REVIEW_NOT_FOUND)
        return product

    def create_product_review(self, data: dict) -> ProductReviewDataObjects:
        productReview = self.repository.get_product_review_by_product_user(user=data.user, product=data.product)
        if productReview:
            return ProductReviewError(ProductReviewError.Code.PRODUCT_REVIEW_EXISTS_WITH_SAME_USER_AND_COMMENT)
        productReview = self.repository.create_product_review(data)
        return productReview

    def update_product_review(self, data: dict, user: User) -> ProductReviewDataObjects:
        product_review_to_update = self.repository.get_product_review_by_product_user(user=user, product=data.product)
        if not product_review_to_update:
            return ProductReviewError(ProductReviewError.Code.PRODUCT_REVIEW_NOT_FOUND)
        updated_product_review = self.repository.update_product_review(data, product_review_to_update)
        return updated_product_review

    def delete_product_review(self, productReview: ProductReview) -> None:
        self.repository.delete_product_review(productReview)


class ProductVariantError(Exception):
    class Code:
        PRODUCT_VARIANT_NOT_FOUND = "PRODUCT_VARIANT_NOT_FOUND"
        PRODUCT_VARIANT_EXISTS_WITH_SAME_USER_AND_COMMENT = "PRODUCT_VARIANT_EXISTS_WITH_SAME_USER_AND_COMMENT"


class ProductVariantService:
    def __init__(
        self,
        repository: ProductVariantRepository,
    ) -> None:
        self.repository = repository

    def get_all_product_variants(self) -> List[ProductVariant]:
        return self.repository.get_all_product_variants()

    def get_product_variants(self, product: Product) -> ProductVariantDataObjects:
        product_variants = self.repository.get_product_variants_by_product(product=product)
        if not product_variants:
            return ProductVariantError(ProductVariantError.Code.PRODUCT_VARIANT_NOT_FOUND)
        return product_variants

    def get_product_variant(self, product: Product, color: ProductColor, size: ProductVolume) -> ProductVariantDataObjects:
        product_variant = self.repository.get_product_variant_by_product_color_size(product=product, color=color, size=size)
        if not product_variant:
            return ProductVariantError(ProductVariantError.Code.PRODUCT_VARIANT_NOT_FOUND)
        return product_variant

    def get_product_variant_by_id(self, id: int) -> ProductVariantDataObjects:
        product = self.repository.get_product_variant_by_id(id)
        if not product:
            return ProductVariantError(ProductVariantError.Code.PRODUCT_VARIANT_NOT_FOUND)
        return product

    def create_product_variant(self, data: dict) -> ProductVariantDataObjects:
        product_variant = self.repository.get_product_variant_by_product_color_size(color=data.color, size=data.size, product=data.product)
        if product_variant:
            return ProductVariantError(ProductVariantError.Code.PRODUCT_VARIANT_EXISTS_WITH_SAME_USER_AND_COMMENT)
        product_variant = self.repository.create_product_variant(data)
        return product_variant

    def update_product_variant(self, data: dict) -> ProductVariantDataObjects:
        product_variant_to_update = self.repository.get_product_variant_by_product_color_size(product=data.product, color=data.color, size=data.size)
        if not product_variant_to_update:
            return ProductVariantError(ProductVariantError.Code.PRODUCT_VARIANT_NOT_FOUND)
        updated_product_variant = self.repository.update_product_variant(data, product_variant_to_update)
        return updated_product_variant

    def delete_product_variant(self, productVariant: ProductVariant) -> None:
        self.repository.delete_product_variant(productVariant)


class ProductVariantImageError(Exception):
    class Code:
        PRODUCT_VARIANT_IMAGE_NOT_FOUND = "PRODUCT_VARIANT_IMAGE_NOT_FOUND"
        PRODUCT_VARIANT_IMAGE_EXISTS_WITH_SAME_USER_AND_COMMENT = "PRODUCT_VARIANT_IMAGE_EXISTS_WITH_SAME_USER_AND_COMMENT"


class ProductVariantImageService:
    def __init__(
        self,
        repository: ProductVariantImageRepository,
    ) -> None:
        self.repository = repository

    def get_all_product_variant_images(self) -> List[ProductVariantImage]:
        return self.repository.get_all_product_variant_images()

    def get_product_variant_images_by_product(self, product: Product) -> List[ProductVariantImageDataObjects]:
        product_variant_images = self.repository.get_product_variant_images_by_product(product=product)
        if not product_variant_images:
            return ProductVariantImageError(ProductVariantImageError.Code.PRODUCT_VARIANT_IMAGE_NOT_FOUND)
        return product_variant_images

    def get_product_variant_images(self, product: Product) -> List[ProductVariantDataObjects]:
        product_variant_image = self.repository.get_product_variant_images_by_product(product=product)
        if not product_variant_image:
            return ProductVariantImageError(ProductVariantImageError.Code.PRODUCT_VARIANT_IMAGE_NOT_FOUND)
        return product_variant_image
    
    def get_product_variant_image(self, product: Product, created_at: str) -> ProductVariantDataObjects:
        product_variant_image = self.repository.get_product_variant_image_by_product_created_at(product=product, created_at=created_at)
        if not product_variant_image:
            raise ProductVariantImageError(ProductVariantImageError.Code.PRODUCT_VARIANT_IMAGE_NOT_FOUND)
        return product_variant_image

    def get_product_variant_image_by_id(self, id: int) -> ProductVariantImageDataObjects:
        product_variant_image = self.repository.get_product_variant_image_by_id(id=id)
        if not product_variant_image:
            raise ProductVariantImageError(ProductVariantImageError.Code.PRODUCT_VARIANT_IMAGE_NOT_FOUND)
        return product_variant_image

    def create_product_variant_image(self, data: dict) -> ProductVariantImageDataObjects:
        product_variant_image = self.repository.get_product_variant_image_by_product_created_at(product=data.product, created_at=data.created_at)
        if product_variant_image:
            raise ProductVariantImageError(ProductVariantImageError.Code.PRODUCT_VARIANT_IMAGE_NOT_FOUND)
        new_product_variant_image = self.repository.create_product_variant_image(data=data)
        return new_product_variant_image

    def update_product_variant_image(self, data: dict) -> ProductVariantImageDataObjects:
        product_variant_image_to_update = self.repository.get_product_variant_image_by_product_created_at(product=data.product, created_at=data.created_at)
        if not product_variant_image_to_update:
            raise ProductVariantImageError(ProductVariantImageError.Code.PRODUCT_VARIANT_IMAGE_NOT_FOUND)
        updated_product_variant_image = self.repository.update_product_variant_image(data, product_variant_image_to_update)
        return updated_product_variant_image

    def delete_product_variant_image(self, productVariantImage: ProductVariantImage) -> None:
        self.repository.delete_product_variant_image(productVariantImage)


class ProductColorError(Exception):
    class Code:
        PRODUCT_COLOR_NOT_FOUND = "PRODUCT_COLOR_NOT_FOUND"
        PRODUCT_COLOR_EXISTS_WITH_SAME_NAME = "PRODUCT_COLOR_EXISTS_WITH_SAME_NAME"


class ProductColorService:
    def __init__(
        self,
        repository: ProductColorRepository,
    ) -> None:
        self.repository = repository

    def get_all_product_colors(self) -> List[ProductColor]:
        return self.repository.get_all_product_colors()

    def get_product_colors_name(self, name: str) -> List[ProductColorDataObjects]:
        product_color = self.repository.get_product_color_name(name=name)
        if not product_color:
            raise ProductColorError(ProductColorError.Code.PRODUCT_COLOR_NOT_FOUND)
        return product_color

    def get_product_color_by_id(self, id: int) -> ProductColorDataObjects:
        product_color = self.repository.get_product_color_by_id(id=id)
        if not product_color:
            raise ProductColorError(ProductColorError.Code.PRODUCT_COLOR_NOT_FOUND)
        return product_color

    def create_product_color(self, data: dict) -> ProductColorDataObjects:
        product_color = self.repository.get_product_color_name(name=data.name)
        if product_color:
            raise ProductColorError(ProductColorError.Code.PRODUCT_COLOR_EXISTS_WITH_SAME_NAME)
        new_product_color = self.repository.update_product_color(data=data)
        return new_product_color

    def update_product_color(self, data: dict) -> ProductColorDataObjects:
        product_color_to_update = self.repository.get_product_color_name(name=data.name)
        if not product_color_to_update:
            raise ProductColorError(ProductColorError.Code.PRODUCT_COLOR_NOT_FOUND)
        updated_product_color = self.repository.update_product_color(data, product_color_to_update)
        return updated_product_color

    def delete_product_color(self, productColor: ProductColor) -> None:
        self.repository.delete_product_color(productColor)


class ProductVolumeError(Exception):
    class Code:
        PRODUCT_VOLUME_NOT_FOUND = "PRODUCT_VOLUME_NOT_FOUND"
        PRODUCT_VOLUME_EXISTS_WITH_SAME_VOLUME_TYPE = "PRODUCT_VOLUME_EXISTS_WITH_SAME_VOLUME_TYPE"


class ProductVolumeService:
    def __init__(
        self,
        repository: ProductVolumeRepository,
    ) -> None:
        self.repository = repository

    def get_all_product_volumes(self) -> List[ProductVolume]:
        return self.repository.get_all_product_volumes()

    def get_product_volume(self, volume_type: str) -> List[ProductVolumeDataObjects]:
        product_volume = self.repository.get_product_volume_volume_type(volume_type=volume_type)
        if not product_volume:
            raise ProductVolumeError(ProductVolumeError.Code.PRODUCT_VOLUME_NOT_FOUND)
        return product_volume

    def get_product_volume_by_id(self, id: int) -> List[ProductVolumeDataObjects]:
        product_volume = self.repository.get_product_volume_by_id(id=id)
        if not product_volume:
            raise ProductVolumeError(ProductVolumeError.Code.PRODUCT_VOLUME_NOT_FOUND)
        return product_volume

    def create_product_volume(self, data: dict) -> ProductVolumeDataObjects:
        product_volume = self.repository.get_product_volume_volume_type(volume_type=data.volume_type)
        if product_volume:
            raise ProductVolumeError(ProductVolumeError.Code.PRODUCT_VOLUME_EXISTS_WITH_SAME_VOLUME_TYPE)
        new_product_volume = self.repository.update_product_volume(data=data)
        return new_product_volume

    def update_product_volume(self, data: dict) -> ProductVolumeDataObjects:
        product_volume_to_update = self.repository.get_product_volume_volume_type(volume_type=data.volume_type)
        if not product_volume_to_update:
            raise ProductVolumeError(ProductVolumeError.Code.PRODUCT_VOLUME_NOT_FOUND)
        updated_product_volume = self.repository.update_product_volume(data, product_volume_to_update)
        return updated_product_volume

    def delete_product_volume(self, productVolume: ProductVolume) -> None:
        self.repository.delete_product_volume(productVolume)