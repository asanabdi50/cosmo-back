from rest_framework import serializers
from rest_framework_dataclasses.serializers import DataclassSerializer

from .types import (
    ProductColorDataObjects,
    BrandDataObject,
    ProductDataObject,
    ProductVolumeDataObjects,
    ProductVariantDataObjects,
    ProductVariantImageDataObjects,
    ProductReviewDataObjects
)
from .models import Product , Brand



class ProductVariantSerializer(DataclassSerializer):
    class Meta:
        dataclass = ProductVariantDataObjects


class ProductVariantImageSerializer(DataclassSerializer):
    class Meta:
        dataclass = ProductVariantImageDataObjects


class ProductColorSerializer(DataclassSerializer):
    class Meta:
        dataclass = ProductColorDataObjects


class ProductVolumeSerializer(DataclassSerializer):
    class Meta:
        dataclass = ProductVolumeDataObjects


class BrandSerializer(DataclassSerializer):
    class Meta:
        dataclass = BrandDataObject


class ProductReviewSerializer(DataclassSerializer):
    class Meta:
        dataclass = ProductReviewDataObjects


class ProductSerializer(DataclassSerializer):
    class Meta:
        dataclass = ProductDataObject