import uuid
from typing import ClassVar

from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from marketplace.utils.abstract_models import BaseModel

User = get_user_model()


class Product(BaseModel):
    MALE = "MALE"
    FEMALE = "FEMALE"
    UNISEX = "UNISEX"
    PRODUCT_GENDER_CHOICES: ClassVar[dict] = {
        MALE: "Male",
        FEMALE: "Female",
        UNISEX: "Unisex",
    }
    article = models.SlugField(
        max_length=11,
        unique=True,
        editable=False,
        verbose_name=_("Article"),
    )
    title = models.CharField(
        max_length=255,
        verbose_name=_("Title"),
    )
    description = models.TextField(verbose_name=_("Description"))
    spec_description = models.TextField(
        verbose_name=_("Specification description"),
    )
    gender = models.CharField(
        max_length=11,
        choices=PRODUCT_GENDER_CHOICES,
        default=UNISEX,
    )
    brands = models.ManyToManyField(
        "Brand",
        related_name="%(class)ss",
        verbose_name=_("Brand"),
        blank=True,
    )

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")
        ordering = ("-created_at",)

    def __str__(self):
        return self.title


class ProductVariant(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    product = models.ForeignKey(Product, related_name="%(class)ss", on_delete=models.CASCADE)
    color = models.ForeignKey("ProductColor", related_name="%(class)ss", on_delete=models.CASCADE)
    size = models.ForeignKey("ProductVolume", related_name="%(class)ss", on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    addition_price = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        verbose_name = _("Product Variant")
        verbose_name_plural = _("Product Variants")
        unique_together = ("product", "color", "size")

    def __str__(self):
        return f"{self.product.title} is color {self.color.name},  size {self.size.volume_type}={self.size.volume_value}"


class ProductVariantImage(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    image = models.ImageField(
        upload_to="images/product_variant_images",
        verbose_name=_("Image"),
    )
    product = models.ForeignKey(
        ProductVariant, related_name="%(class)ss", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Created at"))

    class Meta:
        verbose_name = _("Product Variant Image")
        verbose_name_plural = _("Product Variant Images")

    def __str__(self):
        return f"{self.product.product.title}'s {self.image.name} image"


class ProductColor(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    hex_code = models.CharField(
        max_length=50,
        verbose_name=_("Color Hex Code"),
    )
    name = models.CharField(max_length=50, verbose_name=_("Color name"))

    class Meta:
        verbose_name = _("Product Color")
        verbose_name_plural = _("Product Colors")

    def __str__(self):
        return f"{self.hex_code} -- {self.name}"


class ProductVolume(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    volume_type = models.CharField(
        max_length=50,
        verbose_name=_("Volume Type"),
    )
    volume_value = models.CharField(max_length=255, verbose_name=_("Volume Value"))

    class Meta:
        verbose_name = _("Product Volume")
        verbose_name_plural = _("Product Volumes")

    def __str__(self):
        return f"{self.volume_type} -- {self.volume_value}"


class Brand(BaseModel):
    title = models.CharField(
        max_length=255,
        verbose_name=_("Title"),
    )
    logo = models.ImageField(
        upload_to="images/brands",
        verbose_name=_("Logo"),
        blank=True,
        null=True,
    )
    description = models.TextField(verbose_name=_("Description"))

    class Meta:
        verbose_name = _("Brand")
        verbose_name_plural = _("Brands")
        ordering = ("-created_at",)

    def __str__(self):
        return self.title


class ProductReview(BaseModel):
    user = models.ForeignKey(
        User,
        related_name="%(class)ss",
        on_delete=models.CASCADE,
        verbose_name=_("User"),
    )
    product = models.ForeignKey(
        Product,
        related_name="%(class)ss",
        on_delete=models.CASCADE,
        verbose_name=_("Product"),
    )
    rating = models.PositiveIntegerField(
        validators=[
            MinValueValidator(1.0, message=_("Rating must be at least 1.0")),
            MaxValueValidator(5.0, message=_("Rating must be at most 5.0")),
        ],
        verbose_name=_("Rating"),
    )
    comment = models.TextField(
        verbose_name=_("Text"),
    )

    class Meta:
        verbose_name = _("Product review")
        verbose_name_plural = _("Product reviews")
        ordering = ("-created_at",)

    def __str__(self):
        return f"{self.user} - {self.product}"


class Order(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    buyer = models.ForeignKey(User, related_name="%(class)ss", verbose_name="buyer order", on_delete=models.CASCADE)
    details = models.TextField(blank=True, null=True, verbose_name="detail")
    delivery = models.CharField(verbose_name='Delivery', max_length=255)
    
    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")
    
    def __str__(self) -> str:
        return f"{self.buyer} - {self.product}"


class OrderItem(models.Model):
    order = models.ForeignKey("Order", related_name="%(class)s", verbose_name="order", on_delete=models.CASCADE )
    product_variant = models.ForeignKey("ProductVariant", related_name="%(class)ss", verbose_name="order product",on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1, verbose_name="quantity")
    address = models.CharField(max_length=255, verbose_name="address")
    total_price = models.IntegerField(default=0, verbose_name="total price")

    class Meta:
        verbose_name = _("Order Item")
        verbose_name_plural = _("Order Items")
    
    def __str__(self) -> str:
        return f"{self.order} - {self.product_variant}"
