from cmath import e
from dataclasses import asdict
from typing import Any

from rest_framework import permissions, views
from rest_framework.response import Response
from dependency_injector.wiring import Provide, inject


from .containers import Container
from .serializers import (
    ProductVariantSerializer, 
    ProductVariantImageSerializer,
    ProductColorSerializer,
    ProductVolumeSerializer,
    ProductReviewSerializer,
    ProductSerializer,
    BrandSerializer
)
from .service import (
    ProductService,
    ProductReviewService,
    ProductVariantService,
    ProductVariantImageService,
    ProductColorService,
    ProductVolumeService
)
from .models import (
    Product
)
from django.contrib.auth import get_user_model

User = get_user_model()


class ProductAPIView(views.APIView):
    @inject
    def __init__(
        self,
        _service: ProductService = Provide[Container.product_package.product_service],
        **kwargs: Any,
    ) -> None:
        self._service = _service
        super().__init__(**kwargs)

    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product = self.service.create_product(serializer.validated_data)
            serializer = ProductSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            product = self.service.get_product(request.product.title)
            serializer = ProductSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def put(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data_kwargs = {
                field: serializer.validated_data.get(field) for field in serializer.fields
            }
            product_data = ProductSerializer(**product_data_kwargs)
            product = self.service.update_product(product_data, request.product.title)
            serializer = ProductSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def delete(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            product = self.service.get_product(request.product.title)
            self.service.delete_product(product)
            return Response({"success"})
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get_permissions(self):
        if self.request.method in ["POST", "PUT", "DELETE"]:
            return [permissions.IsAuthenticated()]
        return [permissions.AllowAny()]


class ProductReviewAPIView(views.APIView):
    def __init__(
        self,
        service: ProductReviewService,
        **kwargs: Any,
    ) -> None:
        self.service = service
        super().__init__(**kwargs)

    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductReviewSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data = ProductReviewSerializer(**serializer.validated_data)
            product = self.service.create_product_review(product_data)
            serializer = ProductReviewSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get(self, request, *args, **kwargs):
        try:
            if request.query_params:
                product_id = request.query_params.get('product', None)
                user_email = request.user.email
                if product_id and user_email:
                    user = User.objects.get(email=user_email)
                    product = Product.objects.get(id=id)
                    product_review = self.service.get_product_review(user=user, product=product)
                    if product_review:
                        serializer = ProductReviewSerializer(data=product_review)
                        serializer.is_valid(raise_exception=True)
                        return Response(serializer.data, status=200)
                    else:
                        return Response({'message': "Post review with this user email and product id was not found"}, status=404)
                else:
                    return Response({'message': 'Excepted user email or product id'}, status=400)
            else:
                product_reviews = self.service.get_all_productReviews()
                serializer = ProductReviewSerializer(product_reviews, many=True)
                serializer.is_valid(raise_exception=True)
                return Response(serializer.data, status=200)
        except Exception as e:
            return Response({'error': str(e)}, status=400)

    def put(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductReviewSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data_kwargs = {
                field: serializer.validated_data.get(field) for field in serializer.fields
            }
            product_data = ProductReviewSerializer(**product_data_kwargs)
            product = self.service.update_product_review(product_data, request)
            serializer = ProductReviewSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def delete(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            product = self.service.get_product_review(request.data.user, request.data.product)
            self.service.delete_product_review(product)
            return Response({"success"})
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get_permissions(self):
        if self.request.method in ["POST", "PUT", "DELETE"]:
            return [permissions.IsAuthenticated()]
        return [permissions.AllowAny()]


class ProductVariantAPIView(views.APIView):
    def __init__(
        self,
        service: ProductVariantService,
        **kwargs: Any,
    ) -> None:
        self.service = service
        super().__init__(**kwargs)

    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductVariantSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data = ProductVariantSerializer(**serializer.validated_data)
            product = self.service.create_product_variant(product_data)
            serializer = ProductVariantSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data, status=200)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get(self, request, *args, **kwargs):
        try:
            if request.query_params:
                product_id = request.query_params.get('product', None)
                color_id = request.query_params.get('color', None)
                volume_id = request.query_params.get('volume', None)
                if product_id and color_id and volume_id:
                    product = Product.objects.get(id=id)
                    product_variant = self.service.get_product_variant(color=color_id, size=volume_id, product=product)
                    if product_variant:
                        serializer = ProductVariantSerializer(data=product_variant)
                        serializer.is_valid(raise_exception=True)
                        return Response(serializer.data, status=200)
                    else:
                        return Response({'message': "Post variant with this color, size and product id was not found"}, status=404)
                elif product_id:
                    product = Product.objects.get(id=id)
                    product_variant = self.service.get_product_variants(product=product)
                    serializer = ProductVariantSerializer(data=product_variant)
                    serializer.is_valid(raise_exception=True)
                    return Response(serializer.data, status=200)
                else:
                    return Response({'message': 'Exceped size_id, color_id or at least product_id'}, status=400)
            else:
                product_variants = self.service.get_all_product_variants()
                serializer = ProductVariantSerializer(product_variants, many=True)
                serializer.is_valid(raise_exception=True)
                return Response(serializer.data, status=200)
        except Exception as e:
            return Response({'error': str(e)}, status=400)

    def put(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductVariantSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data_kwargs = {
                field: serializer.validated_data.get(field) for field in serializer.fields
            }
            product_data = ProductVariantSerializer(**product_data_kwargs)
            product = self.service.update_product_variant(product_data, request)
            serializer = ProductVariantSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def delete(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            product = self.service.get_product_variant(color=request.data.color, size=request.data.volume, product=request.data.product)
            self.service.delete_product_variant(product)
            return Response({"success"})
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get_permissions(self):
        if self.request.method in ["POST", "PUT", "DELETE"]:
            return [permissions.IsAuthenticated()]
        return [permissions.AllowAny()]


class ProductVariantImageAPIView(views.APIView):
    def __init__(
        self,
        service: ProductVariantImageService,
        **kwargs: Any,
    ) -> None:
        self.service = service
        super().__init__(**kwargs)

    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductVariantImageSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data = ProductVariantImageSerializer(**serializer.validated_data)
            product = self.service.create_product_variant_image(product_data)
            serializer = ProductVariantImageSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data, status=200)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get(self, request, *args, **kwargs):
        try:
            if request.query_params:
                product_id = request.query_params.get('product', None)
                created_at = request.query_params.get('created_at', None)
                if product_id and created_at:
                    product = Product.objects.get(id=id)
                    product_variant_image = self.service.get_product_variant_image(created_at=created_at, product=product)
                    if product_variant_image:
                        serializer = ProductVariantImageSerializer(data=product_variant_image)
                        serializer.is_valid(raise_exception=True)
                        return Response(serializer.data, status=200)
                    else:
                        return Response({'message': "Post variant image was not found"}, status=404)
                else:
                    return Response({'message': 'Exceped created_at and product_id'}, status=400)
            else:
                product_variant_images = self.service.get_all_product_variant_images()
                serializer = ProductVariantImageSerializer(product_variant_images, many=True)
                serializer.is_valid(raise_exception=True)
                return Response(serializer.data, status=200)
        except Exception as e:
            return Response({'error': str(e)}, status=400)

    def put(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductVariantImageSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data_kwargs = {
                field: serializer.validated_data.get(field) for field in serializer.fields
            }
            product_data = ProductVariantImageSerializer(**product_data_kwargs)
            product = self.service.update_product_variant_image(product_data, request)
            serializer = ProductVariantImageSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def delete(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            product = self.service.get_product_variant_image(created_at=request.data.created_at, product=request.data.product)
            self.service.delete_product_variant_image(product)
            return Response({"success"})
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get_permissions(self):
        if self.request.method in ["POST", "PUT", "DELETE"]:
            return [permissions.IsAuthenticated()]
        return [permissions.AllowAny()]


class ProductColorAPIView(views.APIView):
    def __init__(
        self,
        service: ProductColorService,
        **kwargs: Any,
    ) -> None:
        self.service = service
        super().__init__(**kwargs)

    def post(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductColorSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data = ProductColorSerializer(**serializer.validated_data)
            product = self.service.create_product_color(product_data)
            serializer = ProductColorSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data, status=200)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get(self, request, *args, **kwargs):
        try:
            if request.query_params:
                name = request.query_params.get('name', None)
                if name:
                    product = Product.objects.get(id=id)
                    product_color = self.service.get_product_colors_name(name=name)
                    if product_color:
                        serializer = ProductColorSerializer(data=product_color)
                        serializer.is_valid(raise_exception=True)
                        return Response(serializer.data, status=200)
                    else:
                        return Response({'message': "Post color was not found"}, status=404)
                else:
                    return Response({'message': 'Exceped name'}, status=400)
            else:
                product_colors = self.service.get_all_product_colors()
                serializer = ProductColorSerializer(product_colors, many=True)
                serializer.is_valid(raise_exception=True)
                return Response(serializer.data, status=200)
        except Exception as e:
            return Response({'error': str(e)}, status=400)

    def put(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            serializer = ProductColorSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data_kwargs = {
                field: serializer.validated_data.get(field) for field in serializer.fields
            }
            product_data = ProductColorSerializer(**product_data_kwargs)
            product = self.service.update_product_color(product_data, request)
            serializer = ProductColorSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def delete(self, request, *args: tuple, **kwargs: dict) -> Response:
        try:
            product = self.service.get_product_colors_name(name=request.data.name)
            self.service.delete_product_color(product)
            return Response({"success"})
        except Exception as e:
            return Response({"error": str(e)}, status=400)

    def get_permissions(self):
        if self.request.method in ["POST", "PUT", "DELETE"]:
            return [permissions.IsAuthenticated()]
        return [permissions.AllowAny()]
    

class ProdcutVolumeAPIVIew(views.APIView):
    def __init__(
        self,
        service: ProductVolumeService,
        **kwargs: Any,
    ) -> None:
        self.service = service
        super().__init__(**kwargs)

    def get(self, request, pk=None, *args, **kwargs):
        try:
            if pk:
                product_volume = self.service.get_product_volume_by_id(id=pk)
                serializer = ProductVolumeSerializer(data=product_volume)
                serializer.is_valid(raise_exception=True)
                return Response(serializer.data, status=200)
            else:
                product_colors = self.service.get_all_product_colors()
                serializer = ProductColorSerializer(product_colors, many=True)
                serializer.is_valid(raise_exception=True)
                return Response(serializer.data, status=200)
        except Exception as e:
            return Response({'error': str(e)}, status=400)
    
    def post(self, request, *args, **kwargs):
        try:
            serializer = ProductColorSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            product_data = ProductColorSerializer(**serializer.validated_data)
            product = self.service.create_product_color(product_data)
            serializer = ProductColorSerializer(data=asdict(product))
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data, status=200)
        except Exception as e:
            return Response({"error": str(e)}, status=400)
