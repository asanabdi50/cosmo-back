from django.urls import include, path


urlpatterns = [
    path("products/", include("marketplace.products.urls")),
]
