import logging
import os

from celery import Celery
from dotenv import load_dotenv

load_dotenv()
env = os.getenv("DJANGO_SETTINGS")
settings_module = f"cosmo_core.settings.{env}"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)

logging.basicConfig(level=logging.INFO)

app = Celery("cosmo_core")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    logging.info("Request: %s", self.request)
