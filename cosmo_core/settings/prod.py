from cosmo_core.settings.base import *  # noqa: F403

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.getenv("POSTGRES_DB"),  # noqa: F405
        "USER": os.getenv("POSTGRES_USER"),  # noqa: F405
        "PASSWORD": os.getenv("POSTGRES_PASSWORD"),  # noqa: F405
        "HOST": os.getenv("POSTGRES_HOST"),  # noqa: F405
        "PORT": os.getenv("POSTGRES_PORT"),  # noqa: F405
    }
}
