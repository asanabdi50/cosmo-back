# Main dependencies
Django~=5.0.1
dependency-injector~=4.41.0
dataclasses~=0.6
djangorestframework~=3.14.0
djangorestframework-simplejwt~=5.3.1
drf-yasg~=1.21.7
django-cors-headers~=4.3.1
celery~=5.3.6
redis~=5.0.1
google-api-python-client~=2.114.0
Pillow~=10.2.0
django-mptt~=0.16.0


# .env parser
python-dotenv~=1.0.0
